import { useRouter } from 'next/router'

export default function Index() {
  const router = useRouter()

  return (
    <div style={{ width: '100vw', position: 'relative', height: '100vh', backgroundImage: 'url("/logo.png")', backgroundSize: 'contain', backgroundRepeat: 'no-repeat', backgroundColor: 'black' }}>
      {router.query['production-deploy-hook'] ? (
        <button>
          Hook it up!
        </button>
      ) : (
        <a target="_blank" href="https://vercel.com/import/git?s=github.com%2Frdev%2Finsaniy&c=1&production-deploy-hook=Insanity">
          Import us!
        </a>
      )}
      <style jsx global>
        {`
          body {
            margin: 0;
            font-family: Comic Sans MS;
          }

          * {
            color: black;
            text-decoration: none;
          }

          button, a {
            position: absolute;
            left: 50vw;
            bottom: 120px;
            padding: 12px 28px;
            background: white;
            border-radius: 6px;
            outline: 0;
            font-size: 18px;
            margin-left: -50px;
          }
        `}
      </style>
    </div>
  )
}